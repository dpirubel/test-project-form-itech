<div class="bg-light border-right" id="sidebar-wrapper">
  <div class="sidebar-heading">Itech-Test-Project </div>
  <div class="list-group list-group-flush">
    <a href="{{url('/')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu()}}">Home</a>
    {{-- Qustion 1  --}}
    <a href="{{url('second-buyer-eloquent')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('second-buyer-eloquent')}}">Q&A: 1-1A-1</a>
    <a href="{{url('second-buyer-no-eloquent')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('second-buyer-no-eloquent')}}">Q&A: 1-1A-2</a>
    <a href="{{url('purchase-list-eloquent')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('purchase-list-eloquent')}}">Q&A: 1-1B-1</a>
    <a href="{{url('purchase-list-no-eloquent')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('purchase-list-no-eloquent')}}">Q&A: 1-1B-2</a>
    {{-- Qustion 2  --}}
    <a href="{{url('record-transfer')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('record-transfer')}}">Q&A: 2</a>
    {{-- Qustion 3  --}}
    <a href="{{url('define-callback-js')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('define-callback-js')}}">Q&A: 3</a>
    {{-- Qustion 4  --}}
    <a href="{{url('sort-js')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('sort-js')}}">Q&A: 4-1</a>
    <a href="{{url('foreach-js')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('foreach-js')}}">Q&A: 4-2</a>
    <a href="{{url('filter-js')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('filter-js')}}">Q&A: 4-3</a>
    <a href="{{url('map-js')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('map-js')}}">Q&A: 4-4</a>
    <a href="{{url('reduce-js')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('reduce-js')}}">Q&A: 4-5</a>
    {{-- Qustion 5  --}}
    <a href="{{url('animation')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('animation')}}">Q&A: 5</a>
    {{-- Qustion 6  --}}
    <a href="{{url('i-m-funny')}}" class="list-group-item list-group-item-action bg-light {{Custom::active_menu('i-m-funny')}}">Q&A: 6</a>
	{{-- credits  --}}
    <a href="#" class="list-group-item list-group-item-action bg-light" id="credits">Credits</a>
  </div>
</div>
