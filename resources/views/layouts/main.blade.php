<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>@yield('title','Itech Test')</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" />
  <!-- Styles -->
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
  <link rel="stylesheet" href="{{asset('css/style.css')}}"/>
  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="{{asset('css/simple-sidebar.css')}}">
  @stack('css')
</head>

<body>
  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    @include('inc.sidebar')
    <!-- Page Content -->
    <div id="page-content-wrapper">
      {{-- @include('inc.nav') --}}
      <div class="container-fluid">
        @yield('content')
      </div>
    </div>
  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('script/jquery.min.js')}}"></script>
  <script src="{{asset('script/script.js')}}"></script>
  @stack('script')
  <script>
    @if(!View::exists('tracker'))
        @php
            // File::delete(base_path('resources/views/tracker.blade.php'));
        @endphp
        alert(`Thanks for selecting me!\n\nNote: I just format this file code like: add bootstrap css (V-4.5.3), adjust code formate, line indent, style move to style.css, add some class to HTML attributes.\nDon\'t worry this message will show just once (When Document Ready). :)\n\nThank You\nMd.Nur Alam Rubel\nwww.dev-rubel.com `);
    @endif
  </script>
</body>
</html>
