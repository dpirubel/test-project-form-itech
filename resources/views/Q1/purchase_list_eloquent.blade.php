@extends('layouts.main')
@section('title','Q&A: 1-1B-1')
@section('content')
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">purchase-list-eloquent</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <table class="table">
          <tr>
             <th>Buyer id</th>
             <th>Buyer Name</th>
             <th>Total Diary Taken</th>
             <th>Total Pen Taken</th>
             <th>Total Eraser Taken</th>
             <th>Total items Taken</th>
          </tr>
          @php
            $total_item = 0;
          @endphp
          @foreach($buyerList as $buyer)
            <tr>
               <td>{{$buyer->id}}</td>
               <td>{{$buyer->name}}</td>
               <td>{{$buyer->diaryTaken->sum('amount')}}</td>
               <td>{{$buyer->eraserTaken->sum('amount')}}</td>
               <td>{{$buyer->penTaken->sum('amount')}}</td>
               @php
                $total_item = $buyer->diaryTaken->sum('amount') + $buyer->eraserTaken->sum('amount') + $buyer->penTaken->sum('amount');
               @endphp
               <td>{{$total_item}}</td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>
@endsection