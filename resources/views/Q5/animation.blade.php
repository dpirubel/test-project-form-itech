@extends('layouts.main')
@section('title','Q&A: 5')
@push('css')
	<style>
		#chatdiv {
		    width: 200px;
		    height: 200px;
		    position: absolute;
		    bottom: 0px;
		    right: 0px;
		    background-color: #FF9933;
		}
	</style>
@endpush
@section('content')
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">Animate a box from the top-left edge of window to the bottom-right edge of window, which will return by the same way from the edge. (use js)</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
      		<div id="chartSection" style="width: 500px; border: 1px solid; position: relative;">
	            <div id="chatdiv"></div>
		    </div>
      </div>
    </div>

    @push('script')
    	<script>

			var chatWindow = $("#chatdiv");
			var chatWidth = chatWindow.width();
			var chatHeight = chatWindow.height();

			var windowWidth = $(window).width();
			var windowHeight = $(window).height();

			chatWindow.stop().animate({
	             top: 0
	           , left: 0
	        }, 2000);

    	</script>
    @endpush
@endsection