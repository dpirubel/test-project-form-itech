@extends('layouts.main')
@section('title','Q&A: 4-2')
@section('content')
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">sort-js (foreach)</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <pre>
        		var tasks = [ 
			  {			 
			    'name'     : 'Task1',			 
			    'duration' : 120			 
			  },			 
			  {			 
			    'name'     : 'Task2',			 
			    'duration' : 60			 
			  },			 
			  {			 
			    'name'     : 'Task3',			 
			    'duration' : 240			 
			  }			 
			];
			var difficult_tasks = [];
 
			tasks.forEach(function (task) {
			    if (task.duration >= 120) {
			        difficult_tasks.push(task);
			    }
			});
		</pre>
      </div>
    </div>
@endsection