@extends('layouts.main')
@section('title','Q&A: 4-3')
@section('content')
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">filter-js (array filter)</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <pre>
			const numbers = [1, 5, 7, 10, 45, 97];
			const filterNumber = function(number){
				return number > 7;
			}
			const filtered = numbers.filter(filterNumber);
			console.log(filtered);
		</pre>
      </div>
    </div>
@endsection