@extends('layouts.main')
@section('title','Q&A: 4-1')
@section('content')
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">sort-js (array sort)</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <pre>
    		var accessories = ["mouse", "keyboard", "monitor", "pendrive", "speker"];
		accessories.sort();
		</pre>
      </div>
    </div>
@endsection