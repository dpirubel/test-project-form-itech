@extends('layouts.main')
@section('title','Q&A: 3')
@section('content')
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">Define checkAge() function</p>
      </div>
    </div>

    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <pre>
        		var data = {email:'trumpgmail.com', age:70}; // input json.
			checkAge(data, function(email){
				console.log('Email is valid'); // If data.age < 18 it'll log as not valid.
			});
			// callback function
			function checkAge(data, callback) {
				if(!validateEmail(data.email) || data.age < 18) {
					alert('Invalid data.');
				} else {
				    callback(data);
				}
			}
			// check valid email
			function validateEmail(email) {
			        var re = /\S+@\S+\.\S+/;
			        return re.test(email);
			    }
        </pre>
      </div>
    </div>

    @push('script')
    	<script>
			var data = {email:'trumpgmail.com', age:70}; // input json.
			checkAge(data, function(email){
				console.log('Email is valid'); // If data.age < 18 it'll log as not valid.
			});
			// callback function
			function checkAge(data, callback) {
				if(!validateEmail(data.email) || data.age < 18) {
					console.log('Invalid data.');
				} else {
				    callback(data);
				}
			}
			// check valid email
			function validateEmail(email) {
		        var re = /\S+@\S+\.\S+/;
		        return re.test(email);
		    }
    	</script>
    @endpush
@endsection