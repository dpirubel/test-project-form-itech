
<?php $__env->startSection('title','Q&A: 1-1B-2'); ?>
<?php $__env->startSection('content'); ?>
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">purchase-list-no-eloquent</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <table class="table">
          <tr>
             <th>Buyer id</th>
             <th>Buyer Name</th>
             <th>Total Diary Taken</th>
             <th>Total Pen Taken</th>
             <th>Total Eraser Taken</th>
             <th>Total items Taken</th>
          </tr>
          <?php
            $total_item = 0;
          ?>
          <?php $__currentLoopData = $buyerList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $buyer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
               <td><?php echo e($buyer->id); ?></td>
               <td><?php echo e($buyer->name); ?></td>
               <td><?php echo e($buyer->dt_amount); ?></td>
               <td><?php echo e($buyer->pt_amount); ?></td>
               <td><?php echo e($buyer->et_amount); ?></td>
               <?php
                $total_item = $buyer->dt_amount + $buyer->et_amount + $buyer->et_amount;
               ?>
               <td><?php echo e($total_item); ?></td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </table>
      </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH H:\Wamp20\www\test-for-mid-level-developer\resources\views/Q1/purchase_list_no_eloquent.blade.php ENDPATH**/ ?>