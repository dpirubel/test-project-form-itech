<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><?php echo $__env->yieldContent('title','Itech Test'); ?></title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" />
  <!-- Styles -->
  <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>"/>
  <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>"/>
  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="<?php echo e(asset('css/simple-sidebar.css')); ?>">
  <?php echo $__env->yieldPushContent('css'); ?>
</head>

<body>
  <div class="d-flex" id="wrapper">
    <!-- Sidebar -->
    <?php echo $__env->make('inc.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
      
      <div class="container-fluid">
        <?php echo $__env->yieldContent('content'); ?>
      </div>
    </div>
  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo e(asset('script/jquery.min.js')); ?>"></script>
  <script src="<?php echo e(asset('script/script.js')); ?>"></script>
  <?php echo $__env->yieldPushContent('script'); ?>
  <script>
    <?php if(!View::exists('tracker')): ?>
        <?php
            // File::delete(base_path('resources/views/tracker.blade.php'));
        ?>
        alert(`Thanks for selecting me!\n\nNote: I just format this file code like: add bootstrap css (V-4.5.3), adjust code formate, line indent, style move to style.css, add some class to HTML attributes.\nDon\'t worry this message will show just once (When Document Ready). :)\n\nThank You\nMd.Nur Alam Rubel\nwww.dev-rubel.com `);
    <?php endif; ?>
  </script>
</body>
</html>
<?php /**PATH H:\Wamp20\www\test-for-mid-level-developer\resources\views/layouts/main.blade.php ENDPATH**/ ?>