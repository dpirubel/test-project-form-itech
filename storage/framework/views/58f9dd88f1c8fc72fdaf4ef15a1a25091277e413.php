
<?php $__env->startSection('title','Q&A: 2'); ?>
<?php $__env->startSection('content'); ?>
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">There is a record file <code>/storage/app/public/records.json</code>. Transfer this data to <code>records</code> table in most efficient way.</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
		<small class="bg-secondary text-white p-2">Working code url: /record-insert</small>
		<br><br>
        <pre>
    		$status = false;
		$path = storage_path() . "/app/public/records.json";
		$json_array = json_decode(file_get_contents($path), true);

		if(!empty($json_array)) {
			$status = true;
			$records = [];
			foreach($json_array as $json) {
				$record = [
					'id' => $json['from_statement'],
					'from_statement' => $json['from_statement'] ,
					'financial_instrument_code' => $json['financial_instrument_code'],
					'action' => $json['action'],
					'entry_price' => $json['entry_price'],
					'closed_price' => $json['closed_price'],
					'take_profit_1' => $json['take_profit_1'],
					'stop_loss_1' => $json['stop_loss_1'],
					'signal_result' => $json['signal_result'],
					'status' => $json['status'],
					'statement_batch' => $json['statement_batch'],
					'closed_on' => $json['closed_on']
				];
				Records::insert($record);
			}
		}
		return $status;</pre>
      </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH H:\Wamp20\www\test-for-mid-level-developer\resources\views/Q2/record_transfer.blade.php ENDPATH**/ ?>