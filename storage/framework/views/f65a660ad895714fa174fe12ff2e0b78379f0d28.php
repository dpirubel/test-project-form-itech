
<?php $__env->startSection('title','Q&A: 4-4'); ?>
<?php $__env->startSection('content'); ?>
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">map-js (array map)</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <pre>
			var tasks = [ 
			  {			 
			    'name'     : 'Task1',			 
			    'duration' : 120			 
			  },			 
			  {			 
			    'name'     : 'Task2',			 
			    'duration' : 60			 
			  },			 
			  {			 
			    'name'     : 'Task3',			 
			    'duration' : 240			 
			  }			 
			];
			var map = function (array, callback) { 
			    var new_array = [];			 
			    array.forEach(function (element, index, array) {
			       new_array.push(callback(element)); 
			    });			 
			    return new_array;			 
			};
			 
			var task_names = map(tasks, function (task) {			 
			    return task.name;			 
			});
		</pre>
      </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH H:\Wamp20\www\test-for-mid-level-developer\resources\views/Q4/map_js.blade.php ENDPATH**/ ?>