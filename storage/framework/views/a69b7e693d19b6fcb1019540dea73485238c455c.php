
<?php $__env->startSection('title','Q&A: 4-5'); ?>
<?php $__env->startSection('content'); ?>
	<div class="card text-white bg-secondary text-center">
      <div class="card text-white bg-secondary text-center">
	      <div class="card-body">
	        <p class="text-white m-0">reduce-js (array reduce)</p>
	      </div>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <pre>
			var arrays = [[1, 2], [3, 4], [5, 6]];
			var concatenated = arrays.reduce( function (previous, current) {
		        	return previous.concat(current);
			});
		</pre>
      </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH H:\Wamp20\www\test-for-mid-level-developer\resources\views/Q4/reduce_js.blade.php ENDPATH**/ ?>