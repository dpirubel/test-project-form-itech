
<?php $__env->startSection('title','Q&A: 4-1'); ?>
<?php $__env->startSection('content'); ?>
	<div class="card text-white bg-secondary text-center">
      <div class="card-body">
        <p class="text-white m-0">sort-js (array sort)</p>
      </div>
    </div>
    <div class="row align-items-center my-5">
      <div class="offset-md-1 col-md-10">
        <pre>
    		var accessories = ["mouse", "keyboard", "monitor", "pendrive", "speker"];
		accessories.sort();
		</pre>
      </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH H:\Wamp20\www\test-for-mid-level-developer\resources\views/Q4/sort_js.blade.php ENDPATH**/ ?>