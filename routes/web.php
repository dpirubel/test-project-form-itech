<?php

use Illuminate\Support\Facades\Route;

/* Default Route (Qustion Bank) */
Route::get('/', function () {
    return view('qustion_bank');
});

/* Qustion 1 Routes */
Route::get('second-buyer-eloquent', 'Q1Controller@secondBuyerEloquent')->name('second-buyer-eloquent');
Route::get('second-buyer-no-eloquent', 'Q1Controller@secondBuyerNoEloquent')->name('second-buyer-no-eloquent');
Route::get('purchase-list-eloquent', 'Q1Controller@purchaseListEloquent')->name('purchase-list-eloquent');
Route::get('purchase-list-no-eloquent', 'Q1Controller@purchaseListNoEloquent')->name('purchase-list-no-eloquent');

/* Qustion 2 Routes */
Route::get('record-transfer', 'Q2Controller@recordTransfer')->name('record-transfer');
Route::get('record-insert', 'Q2Controller@recordInsert')->name('record-insert');

/* Qustion 3 Routes */
Route::get('define-callback-js', 'Q3Controller@defineCallbackJs')->name('define-callback-js');

/* Qustion 4 Routes */
Route::get('sort-js', 'Q4Controller@sortJs')->name('sort-js');
Route::get('foreach-js', 'Q4Controller@foreachJs')->name('foreach-js');
Route::get('filter-js', 'Q4Controller@filterJs')->name('filter-js');
Route::get('map-js', 'Q4Controller@mapJs')->name('map-js');
Route::get('reduce-js', 'Q4Controller@reduceJs')->name('reduce-js');

/* Qustion 5 Routes */
Route::get('animation', 'Q5Controller@animation')->name('animation');

/* Qustion 6 Routes */
Route::get('i-m-funny', 'Q6Controller@iMFunny')->name('i-m-funny');