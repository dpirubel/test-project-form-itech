<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('from_statement')
                ->length(1)
                ->default(1)
                ->comment('true means from signal statement')
                ->nullable();
            $table->string('financial_instrument_code', 8)
                ->nullable()
                ->comment('gbpeur');
            $table->enum('action', ['buy', 'sell'])->default('buy')->nullable();
            $table->decimal('entry_price', 19, 8)->nullable();
            $table->decimal('closed_price', 19, 8)->nullable();
            $table->decimal('take_profit_1', 19, 8)->nullable();
            $table->decimal('stop_loss_1', 19, 8)->nullable();
            $table->integer('signal_result')->nullable();
            $table->tinyInteger('status')->length(4)->default(0)->nullable();
            $table->string('statement_batch', 10)->nullable();
            $table->timestamp('closed_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
