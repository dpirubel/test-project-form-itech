<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Q3Controller extends Controller
{	
	/**
	 * [defineCallbackJs description]
	 * @return [type] [description]
	 */
    public function defineCallbackJs()
    {
    	try {
    		return view('Q3.define_callback_js');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }
}
