<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Q5Controller extends Controller
{
	/**
	 * [animation description]
	 * @return [type] [description]
	 */
    public function animation()
    {
    	try {
    		return view('Q5.animation');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }
}
