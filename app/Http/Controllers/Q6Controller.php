<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Q6Controller extends Controller
{
	/**
	 * [iMFunny description]
	 * @return [type] [description]
	 */
    public function iMFunny()
    {
    	try {
    		return view('Q6.i_m_funny');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }
}
