<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Q4Controller extends Controller
{
    /**
     * [sortJs description]
     * @return [type] [description]
     */
    public function sortJs()
    {
    	try {
    		return view('Q4.sort_js');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }

    /**
     * [foreachJs description]
     * @return [type] [description]
     */
    public function foreachJs()
    {
    	try {
    		return view('Q4.foreach_js');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }

    /**
     * [filterJs description]
     * @return [type] [description]
     */
    public function filterJs()
    {
        try {
            return view('Q4.filter_js');
        } catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * [mapJs description]
     * @return [type] [description]
     */
    public function mapJs()
    {
    	try {
    		return view('Q4.map_js');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }

    /**
     * [reduceJs description]
     * @return [type] [description]
     */
    public function reduceJs()
    {
    	try {
    		return view('Q4.reduce_js');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }
}
