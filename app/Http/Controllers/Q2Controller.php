<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Http\Request;

class Q2Controller extends Controller
{	
	/**
	 * [recordTransfer description]
	 * @return [type] [description]
	 */
    public function recordTransfer()
	{
		try {
			return view('Q2.record_transfer');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
	}

	/**
	 * [recordInsert description]
	 * @return [type] [description]
	 */
	public function recordInsert()
	{
		$status = false;
		$path = storage_path() . "/app/public/records.json";
		$json_array = json_decode(file_get_contents($path), true);

		if(!empty($json_array)) {
			$status = true;
			$records = [];
			foreach($json_array as $json) {
				$record = [
					'id' => $json['from_statement'],
					'from_statement' => $json['from_statement'] ,
					'financial_instrument_code' => $json['financial_instrument_code'],
					'action' => $json['action'],
					'entry_price' => $json['entry_price'],
					'closed_price' => $json['closed_price'],
					'take_profit_1' => $json['take_profit_1'],
					'stop_loss_1' => $json['stop_loss_1'],
					'signal_result' => $json['signal_result'],
					'status' => $json['status'],
					'statement_batch' => $json['statement_batch'],
					'closed_on' => $json['closed_on']
				];
				Records::insert($record);
			}
		}
		return $status;
	}
}
