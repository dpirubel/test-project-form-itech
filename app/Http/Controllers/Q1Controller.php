<?php

namespace App\Http\Controllers;

use DB;
use App\Buyers;
use Illuminate\Http\Request;

class Q1Controller extends Controller
{
    /**
     * [secondBuyerEloquent description]
     * @return [type] [description]
     */
    public function secondBuyerEloquent()
    {
    	try {
    		return view('Q1.second_buyer_eloquent');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }

    /**
     * [secondBuyerNoEloquent description]
     * @return [type] [description]
     */
    public function secondBuyerNoEloquent()
    {
    	try {
    		return view('Q1.second_buyer_no_eloquent');
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }

    /**
     * [purchaseListEloquent description]
     * @return [type] [description]
     */
    public function purchaseListEloquent()
    {
    	try {
            $buyerList = Buyers::get();
    		return view('Q1.purchase_list_eloquent', compact('buyerList'));
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }

    /**
     * [purchaseListNoEloquent description]
     * @return [type] [description]
     */
    public function purchaseListNoEloquent()
    {
    	try {
            $buyerList = DB::table('buyers as by')
            ->select('by.id', 'by.name', 'dt.amount as dt_amount', 'et.amount as et_amount', 'pt.amount as pt_amount')
            ->leftJoin('diary_taken as dt', 'dt.buyer_id', 'by.id')
            ->leftJoin('eraser_taken as et', 'et.buyer_id', 'by.id')
            ->leftJoin('pen_taken as pt', 'pt.buyer_id', 'by.id')
            ->get();
    		return view('Q1.purchase_list_no_eloquent', compact('buyerList'));
    	} catch(\Exception $e) {
    		return $e->getMessage();
    	}
    }
}
