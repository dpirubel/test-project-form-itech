<?php

namespace App\Helpers;
use Illuminate\Http\Request;

class Custom {

    /* Manu active custion function */
    public static function active_menu($segment='') 
    {
        $current_segment = request()->segment(1);
        $current_segment .= request()->segment(2)!=''?'/'.request()->segment(2):'';
        return $current_segment==$segment?'active':'';
    }
}