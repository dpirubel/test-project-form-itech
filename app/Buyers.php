<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyers extends Model
{
    protected $table = 'buyers';

    public function diaryTaken()
    {
    	return $this->hasMany('App\DiaryTaken','buyer_id','id');
    }

    public function eraserTaken()
    {
    	return $this->hasMany('App\EraserTaken','buyer_id','id');
    }

    public function penTaken()
    {
    	return $this->hasMany('App\PenTaken','buyer_id','id');
    }
}
