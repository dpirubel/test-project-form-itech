# Test Project From Itech (V1) 🛰

### Company Description

iTECH SOFT SOLUTIONS is a Portugal based software firm and a offshoring company with the main services and development department in Khulna, Bangladesh.
More Details Visit: https://itech-softsolutions.com

## Project Usage

1. Clone this repository
2. `composer update`
3. `Config DB info in .env`
4. `php artisan migrate`
5. `php artisan serve`

## Project Stages
### Stage 1 (complete)
1. Create all controller file
2. Create all view file
3. Create all routes
4. Create migration file's
5. Create all models file
6. Modify/Adjust homepage file
### Stage 2 (Pendding)
1. Solve Qustion 1 (1B Complete) (1A No time to complete)
2. Solve Qustion 2 (complete)
3. Solve Qustion 3 (complete)
4. Solve Qustion 4 (complete)
5. Solve Qustion 5 (complete)
6. Solve Qustion 6 (No time to complete)
### Stage 3 (Pendding)
1. Testing (No time to complete)


## Developer

- [devrubel](https://github.com/dev-rubel)
